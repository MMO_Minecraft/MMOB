package fr.froz.spigot.mmo.mmoRessources;

import fr.froz.spigot.mmo.mmoRessources.Animation.FancyChest.AnimationChestType;
import fr.froz.spigot.mmo.mmoRessources.Animation.FancyChest.FancyChest;
import fr.froz.spigot.mmo.mmoRessources.customMob.CustomMob;
import fr.froz.spigot.mmo.mmoRessources.customMob.creatures.EyeKeeper;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Main extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
    }

    @Override
    public void onDisable() {

    }
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("test")) {
            ArrayList<ItemStack> items = new ArrayList<>();
            items.add(new ItemStack(Material.IRON_AXE));
            new FancyChest(new Location(Bukkit.getWorld("world"),3,72,231),items, AnimationChestType.Epic,this);
            CustomMob cm = new EyeKeeper();
            cm.spawn(new Location(Bukkit.getWorld("world"),3,73,231));
            cm.followTarget((Player) sender,0.5);
        }
        return false;
    }


}
