package fr.froz.spigot.mmo.mmoRessources.customMob;


import fr.froz.spigot.mmo.mmoRessources.Util;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.util.EulerAngle;
import org.bukkit.util.Vector;

import java.util.List;


public abstract class CustomMob implements Listener {
    private Double health;
    private ArmorStand armorstand;
    private ItemStack item;
    private double speed;
    private Hitbox hitbox;
    private boolean movable = true;


    /**
     * @param health vie du mob
     * @param item model item
     * @param speed vitesse
     * @param hitbox hitbox
     */
    public CustomMob(Double health, ItemStack item,double speed,Hitbox hitbox) {
        this.health = health;
        this.item = item;
        this.speed = speed;
        this.hitbox = hitbox;
        Bukkit.getPluginManager().registerEvents(this,Bukkit.getPluginManager().getPlugin("MMORessources"));
    }

    /**
     * @param l location
     */
    public void spawn(Location l) {
        ArmorStand am = l.getWorld().spawn(l, ArmorStand.class);
        am.setVisible(false);
        am.setArms(true);
        am.setRightArmPose(new EulerAngle(270* (2.0 * Math.PI) / 360, 0f, 0f));
        am.setItemInHand(item);
        armorstand = am;

    }


    public abstract void onWalkAnimationStart();


    public abstract void onWalkAnimationStop();

    /**
     * @param e Target
     * @param distance distance
     */
    public void followTarget(Entity e,double distance) {
            BukkitScheduler bs = Bukkit.getScheduler();
            bs.scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("MMORessources"), () -> {
                armorstand.teleport(armorstand.getLocation().setDirection(e.getLocation().subtract(armorstand.getLocation()).toVector()));
                if(movable) {
                if (armorstand.getLocation().distance(e.getLocation()) > distance) {
                    if(!Util.getFrontLocation(armorstand).getBlock().getType().equals(Material.AIR)){
                        armorstand.setVelocity(new Vector(armorstand.getVelocity().getX(),0.20,armorstand.getVelocity().getZ()));
                    }
                    Vector vector = e.getLocation().toVector().subtract(armorstand.getLocation().toVector());
                    vector.normalize().multiply(speed);
                    vector.setY(armorstand.getVelocity().getY());
                    onWalkAnimationStart();
                    armorstand.setVelocity(vector);
                } else {
                    onWalkAnimationStop();
                    armorstand.setVelocity(new Vector(0, armorstand.getVelocity().getY(), 0));
                }
                }
            }, 0L, 1L);

    }
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction()==Action.LEFT_CLICK_AIR||e.getAction()==Action.LEFT_CLICK_BLOCK) {
        Entity le = Util.getNearestEntityInSight(e.getPlayer(),hitbox);
        if(le instanceof ArmorStand){
            ArmorStand as =(ArmorStand) le;
           if(as.equals(armorstand)){
               as.setVelocity(new Vector(0,0,0));
              Vector v = armorstand.getLocation().toVector().subtract(e.getPlayer().getLocation().toVector());
              v.normalize();
              v.multiply(0.8);
              v.setY(0.2);
               armorstand.setVelocity(v);
               BukkitScheduler scheduler = Bukkit.getScheduler();
               movable = false;
               scheduler.scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("MMORessources"), () -> movable=true, 10L);
           }
        }
        }
    }

    /**
     * @param health health
     */
    @SuppressWarnings("unused")
    public void setHealth(Double health) {
        this.health = health;
    }

    /**
     * @return actual Health
     */
    @SuppressWarnings("unused")
    public Double getHealth() {
        return health;
    }

    /**
     * @return the fake ArmorStand
     */
    @SuppressWarnings("unused")
    public ArmorStand getArmorstand() {
        return armorstand;
    }

    /**
     * @param item item
     */
    @SuppressWarnings("unused")
    public void setItem(ItemStack item) {
        this.getArmorstand().setItemInHand(item);
        this.item = item;
    }

    /**
     * @param movable if can move
     */
    @SuppressWarnings("unused")
    public void setMovable(boolean movable) {
        this.movable = movable;
    }

    /**
     * @param ea Wanted angle
     */
    @SuppressWarnings("unused")
    public void setPose(EulerAngle ea){
        this.getArmorstand().setRightArmPose(ea);
    }

    /**
     * @param v1 x
     * @param v2 y
     * @param v3 z
     * @return The list of entity in range
     */
    @SuppressWarnings("unused")
    public List<Entity> getNearbyEntities(double v1, double v2, double v3){
        return getArmorstand().getNearbyEntities(v1,v2,v3);
    }
}
