package fr.froz.spigot.mmo.mmoRessources.customMob.creatures;

import fr.froz.spigot.mmo.mmoRessources.customMob.CustomMob;
import fr.froz.spigot.mmo.mmoRessources.customMob.Hitbox;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;

public class EyeKeeper extends CustomMob {

    public EyeKeeper() {
        super(20.0, new ItemStack(Material.BLAZE_ROD),0.2,new Hitbox(1.5,1.5,1.5,1,5));
    }
    @Override
    public void onWalkAnimationStart(){
        this.getArmorstand().setRightArmPose(new EulerAngle(278* (2.0 * Math.PI) / 360, 0f, 0f));

    }
    @Override
    public void onWalkAnimationStop(){
        this.getArmorstand().setRightArmPose(new EulerAngle(270* (2.0 * Math.PI) / 360, 0f, 0f));

    }

}
