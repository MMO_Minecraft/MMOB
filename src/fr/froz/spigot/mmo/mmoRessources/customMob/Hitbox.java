package fr.froz.spigot.mmo.mmoRessources.customMob;
@SuppressWarnings("unused")
public class Hitbox {
   private double x,y,z,centerY,hitDistance;
    /**
     * @param hitDistance distance limite pour frapper un mob
     */
    public Hitbox(double x,double y,double z,double centerY, double hitDistance){
        this.x = x;
        this.y = y;
        this.z = z;
        this.centerY = centerY;
        this.hitDistance = hitDistance;
    }

    /**
     * @return x of Hitbox
     */
    public double getX() {
        return x;
    }
    /**
     * @return y of Hitbox
     */
    public double getY() {
        return y;
    }
    /**
     * @return z of Hitbox
     */
    public double getZ() {
        return z;
    }

    /**
     * @return the center (y)
     */
    public double getCenterY() {
        return centerY;
    }
    /**
     * @return Hit Distance
     */
    public double getHitDistance() {
        return hitDistance;
    }
}
