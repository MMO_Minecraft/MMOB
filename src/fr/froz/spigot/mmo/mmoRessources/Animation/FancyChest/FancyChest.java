package fr.froz.spigot.mmo.mmoRessources.Animation.FancyChest;

import fr.froz.spigot.mmo.mmoRessources.Animation.Animation;
import net.minecraft.server.v1_12_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftItem;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

import static java.lang.Math.cos;
import static java.lang.Math.sin;


public class FancyChest implements Listener {
    private Location chestLocation;
    private ArrayList<ItemStack> itemList;
    private ArrayList<Player> fPlayer = new ArrayList<>();
    private AnimationChestType animationType;
    private  Plugin plugin;
    public FancyChest(Location chestLocation, ArrayList<ItemStack> itemList, AnimationChestType animationType, Plugin plugin){
    this.chestLocation = chestLocation;
    this.itemList = itemList;
    this.animationType = animationType;
    this.plugin = plugin;
    chestLocation.getWorld().getBlockAt(chestLocation).setType(Material.CHEST);
    Bukkit.getPluginManager().registerEvents(this,this.plugin);
    }
    @EventHandler
    public void onInventoryOpenEvent(InventoryOpenEvent e){
        Chest c = (Chest) e.getInventory().getHolder();
        if(c.getLocation().equals(chestLocation)){
            e.setCancelled(true);
            fPlayer.add((Player) e.getPlayer());
            switch (animationType){
                case Instant:
                    for (ItemStack item : itemList) {
                        e.getPlayer().getInventory().addItem(item);
                    }
                case Epic:

                    Location particleLocation = chestLocation.clone();
                    particleLocation.setX(particleLocation.getX() + 0.5);
                    particleLocation.setY(particleLocation.getY()+0.2);
                    particleLocation.setZ(particleLocation.getZ()+0.5);
                    ((Player) e.getPlayer()).playSound(particleLocation,"mmo.openepic",10,1F);
                    new BukkitRunnable(){
                        double phi = 0;
                        public void run(){
                            phi = phi + Math.PI/8;
                            double x, y, z;

                            Location location1 = particleLocation.clone();
                            for (double t = 0; t <= 2*Math.PI; t = t + Math.PI/16){
                                for (double i = 0; i <= 1; i = i + 1){
                                    x = 0.5*(2*Math.PI-t)*0.5*cos(t + phi + i*Math.PI);

                                    z = 0.5*(2*Math.PI-t)*0.5*sin(t + phi + i*Math.PI);
                                    location1.add(x, i, z);
                                    Animation.spawnParticle((Player) e.getPlayer(),EnumParticle.CRIT_MAGIC,location1,0,0,1);
                                    location1.subtract(x,i,z);
                                }

                            }

                            if(phi > 5*Math.PI){
                                Location itemLocation = particleLocation.clone();
                                itemLocation.setY(itemLocation.getY()+1);
                                EntityItem entityItem = new EntityItem(((CraftWorld) itemLocation.getWorld()).getHandle(), itemLocation.getX(), itemLocation.getY(), itemLocation.getZ(), CraftItemStack.asNMSCopy(itemList.get(0)));
                                EntityPlayer nmsPlayer = ((CraftPlayer) e.getPlayer()).getHandle();
                                Packet packet1 = new PacketPlayOutSpawnEntity(entityItem, 2);
                                Packet packet2 = new PacketPlayOutEntityMetadata(entityItem.getId(), entityItem.getDataWatcher(), true);
                                Packet packet3 = new PacketPlayOutEntityVelocity(entityItem.getId(),0,0,0);
                                Packet packet4 = new PacketPlayOutEntityDestroy(entityItem.getId());
                                nmsPlayer.playerConnection.sendPacket(packet1);
                                nmsPlayer.playerConnection.sendPacket(packet2);
                                nmsPlayer.playerConnection.sendPacket(packet3);
                                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                                    nmsPlayer.playerConnection.sendPacket(packet4);
                                    for (ItemStack item : itemList) {
                                        e.getPlayer().getInventory().addItem(item);
                                    }
                                },75L);

                                this.cancel();
                            }
                        }
                    }.runTaskTimer(Bukkit.getPluginManager().getPlugin("MMORessources"),0, 3);

            }

        }
    }
}
